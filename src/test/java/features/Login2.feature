Feature: Create Location
@Regression
Scenario Outline: Creating Location Flow
Given Enter the User Name as <userName> 
And Enter the Password as <password>
And click on the login button
And Click on Admin 
And Click on Location
And Enter Location Name as <Lname>
And Enter Address as <Address>
When click on Submit button
Then Verify Submit as <VSubmit> 

Examples:
|userName|password|Lname|Address|VSubmit|
|DSubramanian@nltechdev.com|password|NLTD New Office|Thoraipakkam|Location has been saved successfully.|






