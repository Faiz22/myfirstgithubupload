Feature: AMS TOOL

@Smoke
Scenario Outline: TC001 Positive Login Flow

And Enter the User Name as <userName> 
And Enter the Password as <password>
When click on the login button
Then verify the login is success as <vName>  
And click on the logout button 

Examples:
|userName|password|vName|
|DSubramanian@nltechdev.com|password|NORTHERN LIGHTS AMS|
|umas@nltechdev.com|password|NORTHERN LIGHTS AMS|
#|aravindhrajb@nltechdev.com|password|NORTHERN LIGHTS AMS|

@Regression
Scenario Outline: TC002 Creating Location Flow
Given Enter the User Name as <userName> 
And Enter the Password as <password>
And click on the login button
And Click on Admin 
And Click on Location
And Enter Location Name as <Lname>
And Enter Address as <Address>
When click on Submit button
Then Verify Submit as <VSubmit> 

Examples:
|userName|password|Lname|Address|VSubmit|
|DSubramanian@nltechdev.com|password|NLTD New Office|Thoraipakkam|Location has been saved successfully.|





