package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
	
	@Before//(value = "@Positive")
	public void beforeScenario(Scenario sc) {
		j++;
		System.out.println(sc.getName());
		System.out.println(sc.getId());
		//ReportName = sc.getName();
		beginResult();
		testCaseName = sc.getName();
		testCaseDesc = sc.getId();
		category = "SMOKE";
		author = "MOHAMMED FAIZAN A";
		startTestCase();
		startApp("chrome", "http://192.168.182.112/AssetManagementUI/#/login");
	}	
	
	@After//(value = "@Positive")
	public void afterScenario(Scenario sc) {
		System.out.println(sc.getStatus());
		System.out.println(sc.isFailed());
		closeAllBrowsers();
		endResult();
	}

}












