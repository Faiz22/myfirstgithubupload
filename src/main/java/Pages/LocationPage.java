package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LocationPage extends ProjectMethods {
	
	public LocationPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//input[@name='Name']")
	WebElement eleName;
	@And("Enter Location Name as (.*)")
	public LocationPage typeName(String data) {
		type(eleName, data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//input[@name='Address']")
	WebElement eleAddress;
	@And("Enter Address as (.*)")
	public LocationPage typeAddress(String data) {
		type(eleAddress, data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//button[@class='btn btn-lg btn-block btn-info']")
	WebElement eleSubmit;
	@When("click on Submit button")
	public LocationPage clickSubmit() {
		click(eleSubmit);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//div[text()=' Location has been saved successfully. ']")
	WebElement eleVerifySubmit;
	@Then("Verify Submit as (.*)")
	public LocationPage VerifySubmit(String data) {
		verifyExactText(eleVerifySubmit, data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//span[text()='Saved Locations']")
	WebElement eleSavedLocations;
	//@And("Enter the User Name as (.*)")
	public LocationPage clickSavedLocations() {
		click(eleSavedLocations);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="(//a[@class='ng-star-inserted'])[4]")
	WebElement eleNext;
	//@And("Enter the User Name as (.*)")
	public LocationPage ClickNext() {
		click(eleNext);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="(//a[@class='btn btn-block btn-danger active'])[2]")
	WebElement eleDel;
	//@And("Enter the User Name as (.*)")
	public LocationPage DelLocations() {
		click(eleDel);
		return this;
	}
	
	@FindBy(how=How.CLASS_NAME,using="btn btn-primary confirmBtn")
	WebElement eleDelCon;
	//@And("Enter the User Name as (.*)")
	public LocationPage DelLocationsCon() {
		click(eleDelCon);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//div[text()=' Location has been deleted successfully. ']")
	WebElement eleDelverify;
	//@And("Enter the User Name as (.*)")
	public LocationPage DelLocationsVerify() {
		click(eleDelverify);
		return this;
	}
	
	
}
