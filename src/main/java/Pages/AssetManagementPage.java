package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class AssetManagementPage extends ProjectMethods {
	
	public AssetManagementPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//div[text()='NORTHERN LIGHTS AMS']")
	WebElement eleVLogin;
	@Then("verify the login is success as (.*)")
	public AssetManagementPage VerifyLogIn(String data) {
		verifyExactText(eleVLogin, data);
		takeSnap();
		return this;
	}

	@FindBy(how=How.XPATH,using="//a[@class='logout']")
	WebElement eleLogOut;
	@And("click on the logout button")
	public LoginPage ClickLogOut() {
		click(eleLogOut);
		takeSnap();
		return new LoginPage();
	}
	
	@FindBy(how=How.XPATH,using="//a[text()=' ADMIN ']")
	WebElement eleAdmin;
	@And("Click on Admin")
	public AssetManagementPage ClickAdmin() {
		click(eleAdmin);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//a[text()=' Locations ']")
	WebElement eleLoc;
	@And("Click on Location")
	public LocationPage ClickLocation() {
		click(eleLoc);
		return new LocationPage();
	}
	
	@FindBy(how=How.XPATH,using="//a[text()=' Asset Categories ']")
	WebElement eleAssetCategory;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage ClickAssetCategory() {
		click(eleAssetCategory);
		return new AssetCatagoriesPage();
	}
	
	
}
