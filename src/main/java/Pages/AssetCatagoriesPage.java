package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class AssetCatagoriesPage extends ProjectMethods {
	
	public AssetCatagoriesPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="//input[@class='form-control ng-untouched ng-pristine ng-invalid']")
	WebElement eleAssetCategoryName;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage typeAssetCategoryName(String data) {
		type(eleAssetCategoryName, data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//button[@class='btn btn-lg btn-block btn-info']")
	WebElement eleASubmit;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage ClickAssetSubmit() {
		click(eleASubmit);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//span[text()='Saved Asset Categories']")
	WebElement eleSavedAssetCategories;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage clickSavedAssetCategories() {
		click(eleSavedAssetCategories);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="(//a[@class='btn btn-block btn-danger active'])[3]")
	WebElement eleDelAssetCategories;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage DelAssetCategories() {
		click(eleDelAssetCategories);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//button[text()='Yes']")
	WebElement eleDelConAssetCategories;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage DelConAssetCategories() {
		click(eleDelConAssetCategories);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//div[text()=' Asset Category has been deleted successfully. ']")
	WebElement eleDelVerifyAssetCategories;
	//@And("Enter the User Name as (.*)")
	public AssetCatagoriesPage DelAssetCategoriesVerify(String data) {
		verifyExactText(eleDelVerifyAssetCategories, data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//div[text()=' Asset Category has been saved successfully. ']")
	WebElement eleVerifyAssetSubmit;
	public AssetCatagoriesPage VerifyAssetSubmit(String data) {
		verifyExactText(eleVerifyAssetSubmit, data);
		return this;
	}
}
