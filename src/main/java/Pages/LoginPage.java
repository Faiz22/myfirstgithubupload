package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.NAME,using="username")
	WebElement eleUserName;
	@And("Enter the User Name as (.*)")
	public LoginPage typeUserName(String data){
		type(eleUserName, data);
		return this;
	}
	
	@FindBy(how=How.NAME,using="password")
	WebElement elePassword;
	@And("Enter the Password as (.*)")
	public LoginPage typePassword(String data) {
		type(elePassword, data);
		return this;
	}
	
	
	@FindBy(how=How.XPATH,using="//button[text()='Login']")
	WebElement eleLogin;
	@When("click on the login button")
	public AssetManagementPage ClickLogin() {
		click(eleLogin);
		takeSnap();
		return new AssetManagementPage();
	}
	
	@And("Take a Snap as (.*)")
	public LoginPage TakeSnap(String data) {
		takeSnap();
		return this;
	}
	

}
