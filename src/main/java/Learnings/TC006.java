package Learnings;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC006 extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC006_Delete_Category";
		testCaseDesc =  "Login to AMS TOOL and Delete_Category";
		category = "SMOKE";
		author = "MOHAMMED FAIZAN A";
		//dataSheetName = "TC001";
	}
	@Test
	public void Login()
	{
		new LoginPage().
		typeUserName("DSubramanian@nltechdev.com").
		typePassword("password").
		ClickLogin().
		ClickAdmin().
		ClickAssetCategory().
		clickSavedAssetCategories().
		DelAssetCategories().
		DelConAssetCategories().
		DelAssetCategoriesVerify("Asset Category has been deleted successfully.");

	}

}
