package Learnings;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC007 extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC005_Create_Category";
		testCaseDesc =  "Login to AMS TOOL and Create Category";
		category = "SMOKE";
		author = "MOHAMMED FAIZAN A";
		//dataSheetName = "TC001";
	}

	@Test
	public void Login()
	{
		new LoginPage().
		typeUserName("DSubramanian@nltechdev.com").
		typePassword("password").
		ClickLogin().
		ClickAdmin().
		ClickAssetCategory().
		typeAssetCategoryName("Others").
		ClickAssetSubmit().
		VerifyAssetSubmit("Asset Category has been saved successfully.");
	}

}
