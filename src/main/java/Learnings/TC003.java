package Learnings;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003 extends ProjectMethods{
	@BeforeTest
	public void setData() {
		ReportName = "TC003";
		testCaseName = "TC003";
		testCaseDesc =  "Login to AMS TOOL";
		category = "SMOKE";
		author = "MOHAMMED FAIZAN A";
	}
	

	@Test
	public void AMSLogin()
	{
		startApp("chrome", "http://192.168.182.112/AssetManagementUI/#/login");
		WebElement eleUser=locateElement("name", "username");
		type(eleUser, "DSubramanian@nltechdev.com");
		takeSnap();
		WebElement elePassword=locateElement("name", "password");
		type(elePassword, "password");
		takeSnap();
		WebElement eleLogin=locateElement("xpath", "//button[text()='Login']");
		click(eleLogin);
		takeSnap();
		//ADMIN
		WebElement eleAdmin=locateElement("xpath", "//a[text()=' ADMIN ']");
		click(eleAdmin);
		takeSnap();
		
		//Location
		WebElement eleLoc=locateElement("xpath","//a[text()=' Locations ']");
		click(eleLoc);
		takeSnap();
		WebElement eleName=locateElement("xpath","//input[@class='form-control ng-untouched ng-pristine ng-invalid']");
		type(eleName,"NLTD NEW OFFICE");
		takeSnap();
		WebElement eleAddress=locateElement("xpath","//input[@class='form-control ng-untouched ng-pristine ng-invalid']");
		type(eleAddress,"THORAIPAKKAM");
		takeSnap();
		WebElement eleSubmit=locateElement("xpath", "//button[@class='btn btn-lg btn-block btn-info']");
		click(eleSubmit);
		takeSnap();
		
/*		//Asset Category
		WebElement eleAssetCategory=locateElement("xpath","//a[text()=' Asset Categories ']");
		click(eleAssetCategory);
		WebElement eleAssetCategoryName=locateElement("xpath","//input[@class='form-control ng-untouched ng-pristine ng-invalid']");
		type(eleAssetCategoryName,"OTHERS");
		WebElement eleASubmit=locateElement("xpath", "//button[@class='btn btn-lg btn-block btn-info']");
		click(eleASubmit);
		
		//Asset types
		WebElement eleAssettype=locateElement("xpath","//a[text()=' Asset Types ']");
		click(eleAssettype);
		WebElement eleAssetCategoryAS=locateElement("id", "inputGroupSelect01");
		selectDropDownUsingText(eleAssetCategoryAS,"OTHERS");
		WebElement eleAssettypeAS=locateElement("xpath","//input[@class='form-control ng-untouched ng-pristine ng-invalid']");
		type(eleAssettypeAS,"STATIONARY");
		WebElement eleSPECNAME=locateElement("id", "newAttributeName");
		type(eleSPECNAME,"Note Pad");
		WebElement eleValueType=locateElement("id", "newAttributeValueType");
		selectDropDownUsingText(eleValueType,"Single");
		WebElement eleADD=locateElement("xpath","//button[text()='Add']");
		click(eleADD);
		WebElement eleATSubmit=locateElement("xpath","//button[@class='btn btn-lg btn-block btn-info']");
		click(eleATSubmit);
		
		//Vendors
		WebElement eleVendors=locateElement("xpath","//a[text()=' Vendors ']");
		click(eleVendors);
		WebElement eleVName=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[1]");
		type(eleVName,"HP");
		WebElement eleVContactPerson=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-valid'])[1]");
		type(eleVContactPerson,"Faizan");
		WebElement eleVAddress=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[2]");
		type(eleVAddress,"Thoraipakkam");
		WebElement eleVAddress2=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-valid'])[2]");
		type(eleVAddress2,"Vijaya Nagar");
		WebElement eleVArea=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[3]");
		type(eleVArea,"Vijaya Nagar");
		WebElement eleVCity=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[4]");
		type(eleVCity,"Chennai");
		WebElement eleVPIN=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[5]");
		type(eleVPIN,"600097");
		WebElement eleVState=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-valid'])[3]");
		type(eleVState,"Tamil Nadu");
		WebElement eleVPIN2=locateElement("xpath","(//input[@class='form-control ng-untouched ng-pristine ng-invalid'])[6]");
		type(eleVPIN,"600097");
		*/
		
		
		
		/*WebElement eleAssetRequest 	=locateElement("xpath", "(//a[@class='nav-link nav-dropdown-toggle'])[1]");
		click(eleAssetRequest);
		WebElement eleRaiseRequest 	=locateElement("xpath", "(//a[@class='nav-link'])[2]");
		click(eleRaiseRequest);
		WebElement eleSelectAssetcategory =locateElement("xpath", "//select[@name='AssetCategoryId']");
		selectDropDownUsingIndex(eleSelectAssetcategory, 1);
		WebElement eleSelectAssetcategory =locateElement("xpath", "//select[@name='AssetCategoryId']");
		selectDropDownUsingIndex(eleSelectAssetcategory, 1);*/
		
	}

}
