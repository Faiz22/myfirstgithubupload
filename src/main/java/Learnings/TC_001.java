package Learnings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class TC_001 {
	@Test
	public void Booking() throws InterruptedException {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(options);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		//Open Chrome Browser and load https://www.zoomcar.com/chennai
		driver.get("https://www.zoomcar.com/chennai");

		// Click on the Start your wonderful journey link
		driver.findElementByLinkText("Start your wonderful journey").click();

		// In the Search page, Click on the any of the pick up point under POPULAR PICK-UP POINTS
		driver.findElementByXPath("(//div[@class='items'])[2]").click();

		// Click on the Next button
		driver.findElementByXPath("//button[text()='Next']").click();
		Thread.sleep(3000);

		// Specify the Start Date as tomorrow Date
		
		// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int t = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(t);


		driver.findElementByXPath("//div[contains(text(),'"+t+"')]").click();
		// Click on the Next Button
		
		driver.findElementByXPath("//button[text()='Next']").click();
		Thread.sleep(3000);
		// Confirm the Start Date and Click on the Done button
		int tt=t+1;
		System.out.println(tt);
		driver.findElementByXPath("//div[contains(text(),'"+tt+"')]").click();
		driver.findElementByXPath("//button[text()='Done']").click();;
		// In the result page, capture the number of results displayed
		List<WebElement> c = driver.findElementsByXPath("//div[@class='car-list-layout']/div[@class='car-listing']");
		int s=c.size();
		System.out.println(s);
		// Find the highest value and report the brand name
		driver.findElementByXPath("//div[text()=' Price: High to Low ']").click();
		Thread.sleep(3000);
		WebElement v = driver.findElementByXPath("(//div[@class='price'])[1]");
		String vv=v.getText().replaceAll("\\D", "");
		System.out.println(vv);
		// click on the Book Now button for it
		driver.findElementByXPath("(//button[text()='BOOK NOW'])[1]").click();
		// Close the Browser
		driver.close();
	}

}
