package Learnings;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005 extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC005_Create_Location";
		testCaseDesc =  "Login to AMS TOOL and Create Location";
		category = "SMOKE";
		author = "MOHAMMED FAIZAN A";
		ReportName = "TC005";
	}

	@Test
	public void Login()
	{
		new LoginPage().
		typeUserName("DSubramanian@nltechdev.com").
		typePassword("password").
		ClickLogin().
		ClickAdmin().
		ClickLocation().
		typeName("NLTD New Office").
		typeAddress("Thoraipakkam").
		clickSubmit().
		VerifySubmit("Location has been saved successfully.");
	}

}
