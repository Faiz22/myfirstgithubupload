package Learnings;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004 extends ProjectMethods {

	@BeforeTest
	public void setData() {
		ReportName = "TC004";
		testCaseName = "TC004";
		testCaseDesc =  "Login to AMS TOOL";
		category = "SMOKE";
		author = "MOHAMMED FAIZAN A";
	}

	@Test
	public void Login()
	{
		new LoginPage().
		typeUserName("DSubramanian@nltechdev.com").
		typePassword("password").
		ClickLogin().ClickLogOut();

	}

}
