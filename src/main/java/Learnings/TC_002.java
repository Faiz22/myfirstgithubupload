package Learnings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class TC_002 {
	@Test
	public void mytra() throws InterruptedException {
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(options);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.myntra.com/");
		
		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();
		
		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();
		
		// Find the count of Jackets
		String leftCount = 
				//driver.findElementByXPath("//input[@value='Jackets']/following:sibling::span")
				driver.findElementByXPath("//span[@class='categories-num']")
				.getText()
				.replaceAll("\\D", ""); //[^a-zA-Z0-9]
		System.out.println(leftCount);

		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//span[@class='horizontal-filters-sub']")
				.getText()
				.replaceAll("\\D", "");//[^0-9]
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		//driver.findElementByXPath("//h3[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
		}

		// Sort them 
		Comparable<String> max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		
		//driver.close();

		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByClassName("brand-more").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@value='Allen Solly']//parent::label").click();
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();
		Thread.sleep(3000);
		
		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : allenSollyPrices) {
			onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
		}

		// Get the minimum Price 
		Comparable<String> min = Collections.min(onlyPrice);

		// Find the lowest priced Allen Solly
		System.out.println(min);

	}
	
}
